# ORID

## O

● Spring Boot Architecture: Today we learned about the MVC three-layer architecture
● Test: Today's testing includes Integration Test and Unit Test, which are slightly different from previous tests. Integration Test is an overall testing of a module, while Unit Test is a small functional test, just like before. 
2.Problem / Confusing / Difficulties
● Today's knowledge density is higher than before and has become more comprehensive, which makes it a bit difficult for me to digest.The increase in homework requires me to spend more time completing it.
3.Other Comments / Suggestion
● The workload is a bit heavy, I hope some repetitive steps can be reduced a bit.

## R
average

## I
I think every kind of test is very important and indispensable. When there is only Integration testing but no unit test, if there is a problem, it is impossible to quickly locate the problem. When there is only unit test but no Integration testing, it is impossible to test the relationship between modules.

## D
I will strengthen my Java technology in the future
