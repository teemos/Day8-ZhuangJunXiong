package com.afs.restapi.Repository;

import com.afs.restapi.model.Employee;
import com.afs.restapi.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private static final List<Employee> employees = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(5L);

    public EmployeeRepository() {
        employees.add(new Employee(1L, "John Smith", 32, "Male", 5000.0));
        employees.add(new Employee(2L, "Jane Johnson", 28, "Female", 6000.0));
        employees.add(new Employee(3L, "David Williams", 35, "Male", 5500.0));
        employees.add(new Employee(4L, "Emily Brown", 23, "Female", 4500.0));
        employees.add(new Employee(5L, "Michael Jones", 40, "Male", 7000.0));
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee insert(Employee newEmployee) {
        newEmployee.setId(atomicID.incrementAndGet());
        employees.add(newEmployee);
        return newEmployee;
    }

    public Employee update(Employee employeeToUpdate, Employee employee) {
        employeeToUpdate.merge(employee);
        return employee;
    }

    public void delete(Long id) {
//        employees.remove(toRemovedEmployee);
        employees.removeIf(employee -> Objects.equals(id, employee.getId()));
    }

    public void clearAll() {
        employees.clear();
    }

    public List<Employee> getByCompanyId(Long id) {
//        return employees.stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
        return employees.stream().filter(employee -> Objects.equals(employee.getCompanyId(),id)).collect(Collectors.toList());
    }
}

