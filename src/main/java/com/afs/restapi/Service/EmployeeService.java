package com.afs.restapi.Service;

import com.afs.restapi.model.Employee;
import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.exception.SalaryIsInvalidException;
import com.afs.restapi.exception.UpdateException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EmployeeService {
    @Resource
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee create(Employee newEmployee) {
        if (newEmployee.isAgeInvalid()) {
            throw new AgeIsInvalidException();
        }
        if (newEmployee.isSalaryInvalid()) {
            throw new SalaryIsInvalidException();
        }
        newEmployee.setActivate(true);
        return employeeRepository.insert(newEmployee);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = findById(id);
        if (employeeToUpdate.getActivate()) {
            return employeeRepository.update(employeeToUpdate, employee);
        }
        throw new UpdateException();

    }

    public void delete(Long id) {
        findById(id).setActivate(false);
        employeeRepository.delete(id);
    }
}
