package com.afs.restapi.Controller;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.Repository.CompanyRepository;
import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.Service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("companies")
public class CompanyController {
    private final CompanyService companyService;
    public CompanyController(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyService = new CompanyService(companyRepository, employeeRepository);
    }


    @GetMapping
    public List<Company> getAllCompany() {
        return companyService.findAllCompany();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyService.findCompanyById(id);
    }


    @GetMapping(params = {"page", "size"})
    public List<Company> findByPage(Integer page, Integer size) {
        return companyService.findByPageAndSize(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company insertCompany(@RequestBody Company company) {
        return companyService.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companyService.updateCompanyById(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.deleteCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return companyService.getEmployeesByCompanyId(id);
    }

}
