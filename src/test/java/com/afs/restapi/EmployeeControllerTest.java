package com.afs.restapi;

import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.model.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanEmployeeData() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployee_given_employees() throws Exception {
        // Given
        Employee john = new Employee(1L, "john", 20, "Female", 8000.0);
        employeeRepository.insert(john);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_employees_filter_by_gender_when_perform_findEmployeesByGender_given_two_employees_with_different_gender() throws Exception {
        // Given
        Employee john = new Employee(1L, "John Smith", 32, "Male", 5000.0);
        Employee jane = new Employee(2L, "Jane Johnson", 28, "Female", 6000.0);
        employeeRepository.insert(john);
        employeeRepository.insert(jane);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Female"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(jane.getId()))
                .andExpect(jsonPath("$[0].name").value(jane.getName()))
                .andExpect(jsonPath("$[0].age").value(jane.getAge()))
                .andExpect(jsonPath("$[0].gender").value(jane.getGender()))
                .andExpect(jsonPath("$[0].salary").value(jane.getSalary()));
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        // Given
//        String johnJson = "{\n" +
//                "    \"name\": \"Nico\",\n" +
//                "    \"age\": 99,\n" +
//                "    \"gender\": \"Male\",\n" +
//                "    \"salary\": 9991\n" +
//                "}";
        Employee john = new Employee(1L, "John Smith", 22, "Male", 5000.0);
        String johnJson = new ObjectMapper().writeValueAsString(john);
        // When
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("John Smith"))
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(5000.0));
    }
    @Test
    void should_return_employee_when_perform_getEmployeeById_given_id() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);
        //when and then
        client.perform(MockMvcRequestBuilders.get("/employees/"+john.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(john.getId()))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value(john.getGender()))
                .andExpect(jsonPath("$.salary").value(john.getSalary()));
    }
    @Test
    void should_return_employee_when_perform_updateEmployee_given_age_and_salary() throws Exception {
        Employee john = new Employee(null, "John Smith", 22, "Male", 5000.0);
        employeeRepository.insert(john);
        john.setAge(25);
        john.setSalary(6000.0);
        john.setActivate(true);

        String johnJson = new ObjectMapper().writeValueAsString(john);
        client.perform(MockMvcRequestBuilders.put("/employees/"+john.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(john.getId()))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value(john.getGender()))
                .andExpect(jsonPath("$.salary").value(john.getSalary()));
    }

    @Test
    void should_return_void_when_perform_deleteEmployee_given_id() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);
        client.perform(MockMvcRequestBuilders.delete("/employees/"+john.getId()))
                .andExpect(status().isNoContent());

    }

    @Test
    void should_return_employees_when_perform_findByPage_given_page_and_size() throws Exception {
        List<Employee> employees = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            Employee employee =new Employee(null, "John Smith", 32, "Male", 5000.0);
            employeeRepository.insert(employee);
            employees.add(employee);
        }
        client.perform(MockMvcRequestBuilders.get("/employees").param("page","1").param("size","5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(5)))
                .andExpect(jsonPath("$[0].id").value(employees.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(employees.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(employees.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(employees.get(3).getId()))
                .andExpect(jsonPath("$[4].id").value(employees.get(4).getId()));
    }
}
