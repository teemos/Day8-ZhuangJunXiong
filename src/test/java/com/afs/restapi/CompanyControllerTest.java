package com.afs.restapi;

import com.afs.restapi.Repository.CompanyRepository;
import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc client;

    @BeforeEach
    void cleanCompanyDate() {
        companyRepository.clearAll();
    }

    @Test
    void should_return_companies_when_getAllCompany_given_companies() throws Exception {
        // Given

        Company aCompany = new Company(null, "ACompany");
        companyRepository.addCompany(aCompany);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(aCompany.getId()))
                .andExpect(jsonPath("$[0].name").value(aCompany.getName()));
    }

    @Test
    void should_return_created_company_when_perform_createCompany_given_a_company_json() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");

        String aCompanyJson = new ObjectMapper().writeValueAsString(aCompany);
        //When
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(aCompanyJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("ACompany"));
    }

    @Test
    void should_return_company_find_by_id_when_perform_findCompanyById_given_a_company_id_and_companies() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");
        Company bCompany = new Company(null, "BCompany");
        companyRepository.addCompany(aCompany);
        companyRepository.addCompany(bCompany);

        //When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}", bCompany.getId())) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(bCompany.getId()))
                .andExpect(jsonPath("$.name").value(bCompany.getName()));
    }

    @Test
    void should_return_updated_company_when_perform_updateCompanyName_given_a_company_id_and_company_json() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");
        companyRepository.addCompany(aCompany);
        String bCompanyJson = new ObjectMapper().writeValueAsString(aCompany);
        aCompany.setName("BCompany");
        //When
        client.perform(MockMvcRequestBuilders.put("/companies/{id}", aCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON).content(bCompanyJson)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(aCompany.getId()))
                .andExpect(jsonPath("$.name").value(aCompany.getName()));
    }

    @Test
    void should_return_null_when_perform_deleteCompany_given_a_company_id() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");
        companyRepository.addCompany(aCompany);
        //When
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", aCompany.getId())) //Then
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_companies_when_perform_findByPageAndSize_given_size_and_page() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");
        Company bCompany = new Company(null, "BCompany");
        Company cCompany = new Company(null, "CCompany");
        companyRepository.addCompany(aCompany);
        companyRepository.addCompany(bCompany);
        companyRepository.addCompany(cCompany);
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.set("page", "1");
        parameters.set("size", "2");

        //When
        client.perform(MockMvcRequestBuilders.get("/companies").params(parameters)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(aCompany.getId()))
                .andExpect(jsonPath("$[0].name").value(aCompany.getName()))
                .andExpect(jsonPath("$[1].id").value(bCompany.getId()))
                .andExpect(jsonPath("$[1].name").value(bCompany.getName()));
    }

    @Test
    void should_return_employees_when_perform_getEmployeesByCompanyId_given_companyId_and_employees() throws Exception {
        //Given
        Company aCompany = new Company(null, "ACompany");
        companyRepository.addCompany(aCompany);
        Employee Tom = new Employee(null, "Tom", 30, "Female", 3000.0, 1L);

        Employee Tim = new Employee(null, "Tim", 22, "Female", 15000.0, aCompany.getId());

        Employee john = new Employee(null, "john", 22, "Female", 12000.0, aCompany.getId());

        employeeRepository.insert(Tim);
        employeeRepository.insert(john);
        employeeRepository.insert(Tom);

        //When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", aCompany.getId()))
        //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(Tim.getId()))
                .andExpect(jsonPath("$[0].name").value(Tim.getName()))
                .andExpect(jsonPath("$[1].id").value(john.getId()))
                .andExpect(jsonPath("$[1].name").value(john.getName()));
    }

}
