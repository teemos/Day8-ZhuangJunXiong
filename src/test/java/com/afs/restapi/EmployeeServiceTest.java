package com.afs.restapi;

import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.Service.EmployeeService;
import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.exception.SalaryIsInvalidException;
import com.afs.restapi.exception.UpdateException;
import com.afs.restapi.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    EmployeeRepository employeeRepositoryMock = mock(EmployeeRepository.class);

    @Test
    void should_return_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "nico", 15, "Male", 4000.0);
        Assertions.assertThrows(AgeIsInvalidException.class, () -> employeeService.create(employee));
    }


    @Test
    void should_return_not_create_successfully_when_create_employee_given_salary_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "nico", 30, "Male", 4000.0);
        Assertions.assertThrows(SalaryIsInvalidException.class, () -> employeeService.create(employee));
    }


    @Test
    void should_return_set_in_activate_when_delete_employee_given_an_activate_employee() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "nico", 30, "Male", 4000.0);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);

        employeeService.delete(1L);

        verify(employeeRepositoryMock, times(1)).delete(1L);
    }

    @Test
    void should_throw_error_when_updateEmployee_given_a_employee_have_not_active() throws Exception {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "John Smith", 20, "Male", 5000.0);
        Employee newEmployee = new Employee(null, "John Smith", 21, "Male", 6000.0);
        employee.setActive(false);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);

        //When
        Assertions.assertThrows(UpdateException.class, () -> employeeService.update(1L, newEmployee));
    }

}
